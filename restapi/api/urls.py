from django.urls import path

from .views import CityView, UniversityView, StudentView

app_name = 'api'
urlpatterns = [
    path('city/', CityView.as_view(), name='city'),
    path('city/<int:city_id>', CityView.as_view(), name='city_with_id'),
    path('university/', UniversityView.as_view(), name='university'),
    path('university/<int:university_id>', UniversityView.as_view(), name='university_with_id'),
    path('student/', StudentView.as_view(), name='student'),
    path('student/<int:student_id>', StudentView.as_view(), name='student_with_id'),
]
