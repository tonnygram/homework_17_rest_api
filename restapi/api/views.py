import json

from django.http import HttpRequest, JsonResponse
from django.utils.datastructures import MultiValueDictKeyError
from django.views.generic.base import View

from .models import City, University, Student

__all__ = {'CityView', 'UniversityView', 'StudentView'}


class CityView(View):
    def get(self, request: HttpRequest, city_id: int = None) -> JsonResponse:
        context = dict()
        if city_id:
            try:
                object_city = City.objects.get(pk=city_id)
            except City.DoesNotExist:
                context['Error'] = f'City with ID {city_id} not found!'
            else:
                context[object_city.id] = object_city.city
        else:
            all_cities = City.objects.all()
            for object_city in all_cities:
                context[object_city.id] = object_city.city
        return JsonResponse({'response': context})

    def post(self, request: HttpRequest) -> JsonResponse:
        context = dict()
        try:
            new_city = request.POST['city']
        except MultiValueDictKeyError:
            context['Error'] = f'The specified key/keys ({request.POST.keys()}) does not exist!'
        else:
            object_city = City(city=new_city)
            object_city.save()
            create_object = City.objects.last()
            context[create_object.id] = create_object.city
        return JsonResponse({'response': context})

    def put(self, request: HttpRequest, city_id: int = None):
        # TODO Работает только если данные передавать в raw в формате: {"city": "Brest2222"}
        # TODO Вот сделал скриншот в Postman: https://ibb.co/dGhjt1Q
        context = dict()
        try:
            json_data = json.loads(request.body.decode("utf-8"))
            name_city_for_update = json_data['city']
            object_city = City.objects.get(pk=city_id)
        except MultiValueDictKeyError:
            context['Error'] = f'The specified key/keys ({json_data}) does not exist!'
        except City.DoesNotExist:
            context['Error'] = f'City with ID {city_id} not found!'
        else:
            object_city.city = name_city_for_update
            object_city.save()
            object_city = City.objects.get(pk=city_id)
            context[object_city.id] = object_city.city
        return JsonResponse({'response': context})

    def delete(self, request: HttpRequest, city_id: int = None):
        context = dict()
        try:
            object_city = City.objects.get(id=city_id)
        except City.DoesNotExist:
            context['Error'] = f'City with ID {city_id} not found!'
        else:
            object_city.delete()
            context['Success'] = f'City with ID {city_id} has delete!'
        return JsonResponse({'response': context})


class UniversityView(View):
    def get(self, request: HttpRequest, university_id: int = None) -> JsonResponse:
        context = dict()
        if university_id:
            try:
                object_university = University.objects.get(pk=university_id)
            except University.DoesNotExist:
                context['Error'] = f'University with ID {university_id} not found!'
            else:
                context[object_university.id] = object_university.university
        else:
            all_universities = University.objects.all()
            for object_university in all_universities:
                context[object_university.id] = object_university.university
        return JsonResponse({'response': context})

    def post(self, request: HttpRequest) -> JsonResponse:
        context = dict()
        try:
            city_id = request.POST['city_id']
            new_university = request.POST['university']
            object_city = City.objects.get(pk=city_id)
        except MultiValueDictKeyError:
            context['Error'] = f'The specified key/keys ({request.POST.keys()}) does not exist!'
        except City.DoesNotExist:
            context['Error'] = f'City with ID {city_id} not found!'
        else:
            object_university = University(city=object_city, university=new_university)
            object_university.save()
            create_object = University.objects.last()
            context[create_object.id] = create_object.university
        return JsonResponse({'response': context})

    def put(self, request: HttpRequest, university_id: int = None):
        # TODO Работает только если данные передавать в raw в формате: {"university": "11111"}
        # TODO Вот сделал скриншот в Postman: https://ibb.co/ynVGPnV
        context = dict()
        try:
            json_data = json.loads(request.body.decode("utf-8"))
            name_university_for_update = json_data['university']
            object_university = University.objects.get(pk=university_id)
        except MultiValueDictKeyError:
            context['Error'] = f'The specified key/keys ({json_data}) does not exist!'
        except University.DoesNotExist:
            context['Error'] = f'University with ID {university_id} not found!'
        else:
            object_university.university = name_university_for_update
            object_university.save()
            object_university = University.objects.get(pk=university_id)
            context[object_university.id] = object_university.university
        return JsonResponse({'response': context})

    def delete(self, request: HttpRequest, university_id: int = None):
        context = dict()
        try:
            object_university = University.objects.get(id=university_id)
        except University.DoesNotExist:
            context['Error'] = f'University with ID {university_id} not found!'
        else:
            object_university.delete()
            context['Success'] = f'University with ID {university_id} has delete!'
        return JsonResponse({'response': context})


class StudentView(View):
    def get(self, request: HttpRequest, student_id: int = None) -> JsonResponse:
        context = dict()
        if student_id:
            try:
                object_student = Student.objects.get(pk=student_id)
            except Student.DoesNotExist:
                context['Error'] = f'Student with ID {student_id} not found!'
            else:
                context[object_student.id] = object_student.student
        else:
            all_students = Student.objects.all()
            for object_student in all_students:
                context[object_student.id] = object_student.student
        return JsonResponse({'response': context})

    def post(self, request: HttpRequest) -> JsonResponse:
        context = dict()
        try:
            university_id = request.POST['university_id']
            new_student = request.POST['student']
            object_university = University.objects.get(pk=university_id)
        except MultiValueDictKeyError:
            context['Error'] = f'The specified key/keys ({request.POST.keys()}) does not exist!'
        except University.DoesNotExist:
            context['Error'] = f'University with ID {university_id} not found!'
        else:
            object_student = Student(university=object_university, student=new_student)
            object_student.save()
            create_object = Student.objects.last()
            context[create_object.id] = create_object.student
        return JsonResponse({'response': context})

    def put(self, request: HttpRequest, student_id: int = None):
        # TODO Работает только если данные передавать в raw в формате: {"student": "555555"}
        # TODO Вот сделал скриншот в Postman: https://ibb.co/ZMh45sY
        context = dict()
        try:
            json_data = json.loads(request.body.decode("utf-8"))
            name_student_for_update = json_data['student']
            object_student = Student.objects.get(pk=student_id)
        except MultiValueDictKeyError:
            context['Error'] = f'The specified key/keys ({json_data}) does not exist!'
        except Student.DoesNotExist:
            context['Error'] = f'Student with ID {student_id} not found!'
        else:
            object_student.student = name_student_for_update
            object_student.save()
            object_student = Student.objects.get(pk=student_id)
            context[object_student.id] = object_student.student
        return JsonResponse({'response': context})

    def delete(self, request: HttpRequest, student_id: int = None):
        context = dict()
        try:
            object_student = Student.objects.get(id=student_id)
        except Student.DoesNotExist:
            context['Error'] = f'Student with ID {student_id} not found!'
        else:
            object_student.delete()
            context['Success'] = f'Student with ID {student_id} has delete!'
        return JsonResponse({'response': context})
