from django.db import models

__all__ = {'City', 'University', 'Student'}


class City(models.Model):
    city: str = models.CharField(max_length=30, help_text='City')

    class Meta:
        verbose_name: str = "City"
        verbose_name_plural: str = "Cities"

    def __str__(self):
        return self.city


class University(models.Model):
    city: str = models.ForeignKey(City, on_delete=models.PROTECT, help_text='City')
    university: str = models.CharField(max_length=100, help_text='University')

    class Meta:
        verbose_name: str = "University"
        verbose_name_plural: str = "Universities"

    def __str__(self):
        return self.university


class Student(models.Model):
    university: str = models.ForeignKey(University, on_delete=models.PROTECT, help_text='University')
    student: str = models.CharField(max_length=75, help_text='Student')

    class Meta:
        verbose_name: str = "Student"
        verbose_name_plural: str = "Students"

    def __str__(self):
        return self.student
