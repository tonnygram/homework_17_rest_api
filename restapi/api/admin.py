from django.contrib import admin

from .models import City, University, Student

__all__ = {'CityAdmin', 'UniversityAdmin', 'StudentAdmin'}


# admin:adminadmin
class CityAdmin(admin.ModelAdmin):
    list_display = ('city',)
    list_display_links = ('city',)
    search_fields = ('city',)


class UniversityAdmin(admin.ModelAdmin):
    list_display = ('university', 'city')
    list_display_links = ('university',)
    search_fields = ('university', 'city')


class StudentAdmin(admin.ModelAdmin):
    list_display = ('student', 'university')
    list_display_links = ('student',)
    search_fields = ('student', 'university')


admin.site.register(City, CityAdmin)
admin.site.register(University, UniversityAdmin)
admin.site.register(Student, StudentAdmin)
